# Generated by Django 3.2.4 on 2021-08-04 08:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blink', '0002_auto_20210713_0542'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='user_id',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='link',
            name='description',
            field=models.CharField(default='', max_length=255),
        ),
    ]
