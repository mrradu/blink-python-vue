import json
from unittest import mock

import requests
from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status


# class CreatePreviewTests(TestCase):

#     def test_create_valid_preview(self):
#         with mock.patch("blink.views.validate_content_type") as MockClass:
#             MockClass.return_value = True
#             response = self.client.post(
#                 reverse("create_preview"),
#                 data=json.dumps({"url": "https://agilix.ru/"}),
#                 content_type="application/json",
#             )
#             self.assertEqual(response.status_code, status.HTTP_201_CREATED)

#     def test_create_invalid_preview(self):
#         response = self.client.post(
#             reverse("create_preview"),
#             data=json.dumps({"url": "http://agilix.ru/"}),
#             content_type="application/json",
#         )
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, status_code, content_length=1):
            self.status_code = status_code
            self.content_length = content_length

    if 'image-is-too-big' in args[0]:
        MockResponse(200, content_length=1048576 + 1)

    if args[0] == 'https://example.com/existing-image.png':
        MockResponse(200)

    return MockResponse(404)


class MyTestClient(Client):

    def __init__(self):
        super().__init__(HTTP_USER_ID=1)


def create_data(title='', url='', image_url='null'):
    return {
        'title': title,
        'url': url,
        'image_url': image_url,
        'description': 'null'
    }


def post_data(client, title='', url='', image_url='null'):
    data = create_data(title=title, url=url, image_url=image_url)
    client.post(reverse('links_create_retrieve'), data=data)


class LinksTests(TestCase):
    client_class = MyTestClient

    def test_user_cannot_create_http_link(self):
        data = create_data(url='http://agilix.ru')

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        body = json.loads(response.content)
        self.assertEqual(body['errors'], {'url': ['url should be HTTPS']})

    def test_user_cannot_create_link_without_description(self):
        data = create_data(title='AgiliX Consulting', url='https://agilix.ru')

        response = self.client.post(reverse('links_create_retrieve'), data=data)
 
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        body = json.loads(response.content)
        self.assertEqual(body['errors'], {'description': ['This field may not be null.']})

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_create_link(self, stub_get):
        data = create_data(
            title='AgiliX Consulting',
            url='https://agilix.ru',
            image_url='https://example.com/existing-image.png',
        )

        response = self.client.post(
            reverse('links_create_retrieve'),
            data=data,
            **{'HTTP_USER_ID': 1},
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['user_id'], 1)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_save_link_when_provided_inaccessible_image(self, stub_get):
        data = create_data(
            title='AgiliX Consulting',
            url='https://agilix.ru',
            image_url='https://example.com/non-existing-image.png',
        )

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['image_url'], 'null')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_save_link_when_provided_large_image(self, stub_get):
        data = create_data(
            title='AgiliX Consulting',
            url='https://agilix.ru',
            image_url='https://example.com/image-is-too-big.png',
        )

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        body = json.loads(response.content)
        self.assertEqual(body['image_url'], 'null')

    def test_user_can_create_link_without_image(self):
        data = create_data(title='AgiliX Consulting', url='https://agilix.ru')

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_link_without_user_id_header(self):
        data = create_data(title='AgiliX Consulting', url='https://agilix.ru')

        response = self.client.post(reverse('links_create_retrieve'), data=data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        body = json.loads(response.content)
        self.assertEqual(body['errors'], {'HTTP_USER_ID': ['This header required']})

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_user_can_get_own_links(self, stub_get):
        post_data(
            client=self.client,
            title='AgiliX Consulting',
            url='https://agilix.ru',
            image_url='https://example.com/existing-image.png',
        )
        post_data(
            client=self.client, 
            title='Google',
            url='https://google.com',
        )

        response = self.client.get(reverse('links_create_retrieve'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        links = json.loads(response.content)
        self.assertEqual(len(links), 2)
        self.assertEqual(links[0]['title'], 'AgiliX Consulting')
        self.assertEqual(links[0]['user_id'], 1)
        self.assertEqual(links[1]['title'], 'Google')
        self.assertEqual(links[1]['user_id'], 1)

    def test_user_cannot_get_someones_else_links(self):
        user_id = 1
        post_data(
            client=self.client,
            title='AgiliX Consulting',
            url='https://agilix.ru',
        )
        other_user_id = 2
        data = create_data(
            title='Google',
            url='https://google.com',
        )
        header = {'HTTP_USER_ID': other_user_id}
        self.client.post(reverse('links_create_retrieve'), data=data, **header)
        header = {'HTTP_USER_ID': user_id}

        response = self.client.get('/links/', **header)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        links = json.loads(response.content)
        self.assertEqual(len(links), 1)
        self.assertEqual(links[0]['title'], 'AgiliX Consulting')
        self.assertEqual(links[0]['user_id'], user_id)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_anyone_can_get_other_users_link_by_id(self, stub_get):
        user_id = 1
        data = {
            'title': 'AgiliX Consulting',
            'url': 'https://agilix.ru',
            'user_id': user_id,
            'image_url': 'https://example.com/existing-image.png',
            'description': 'null'
        }
        self.client.post(reverse('links_create_retrieve'), data=data)
        other_user_id = 2
        data = {
            'title': 'Google',
            'url': 'https://google.com',
            'user_id': other_user_id,
            'image_url': 'null',
            'description': 'null'
        }
        self.client.post(reverse('links_create_retrieve'), data=data, **{'HTTP_USER_ID': other_user_id})
        header = {'HTTP_USER_ID': user_id}

        response = self.client.get('/links/?user_id=2', **header)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        links = json.loads(response.content)
        self.assertEqual(len(links), 1)
        self.assertEqual(links[0]['title'], 'Google')
        self.assertEqual(links[0]['user_id'], 2)
