from django.urls import path

from . import views

urlpatterns = [
    path('links/', views.LinksView.as_view(), name='links_create_retrieve'),
    path('preview/', views.PreviewLinkView.as_view(), name='create_preview'),
]
