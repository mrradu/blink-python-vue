import re

import requests
from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Link
from .serializers import LinkSerializer


class LinksView(APIView):

    def get(self, request):
        if 'user_id' in request.query_params:
            user_id = request.query_params['user_id']
        else:
            if 'user_id' in request.headers:
                user_id = request.headers['user_id']
            else:
                user_id = None

        links = Link.objects.filter(user_id=user_id)
        serializer = LinkSerializer(links, many=True)
        return Response(serializer.data)

    def post(self, request):
        if 'user-id' not in request.headers:
            return JsonResponse({"errors": {'HTTP_USER_ID': ['This header required']}},
                                status=status.HTTP_403_FORBIDDEN)

        image_url = request.data['image_url']
        if image_url != 'null':
            try:
                self.validate_image_url(image_url)
            except RuntimeError as error:
                print(error.args)
                image_url = 'null'

        if image_url != 'null':
            r = requests.get(image_url)

            if r.status_code != 200 or int(r.headers['Content-Length']) > 1048576:
                image_url = 'null'

        try:
            validate_url(request.data.get('url'))
        except ValueError as error:
            return JsonResponse({"errors": {'url': error.args}}, status=status.HTTP_400_BAD_REQUEST)

        data = {
            'title': request.data.get('title'),
            'url': request.data.get('url'),
            'image_url': image_url,
            'description': request.data.get('description'),
            'user_id': request.headers['user-id']
        }

        serializer = LinkSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return JsonResponse({"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def validate_image_url(url):
        validate_url(url)
        if not url.endswith("jpg") and not url.endswith("png"):
            raise ValueError("supported image types are: .jpg and png")
        else:
            return url


def validate_url(url):
    if not url.startswith("https://"):
        raise ValueError("url should be HTTPS")
    else:
        return url


class PreviewLinkView(APIView):

    def post(self, request):
        try:
            r = requests.get(validate_url(request.data.get('url')))
        except ValueError as error:
            return Response({'error': f"Validation: {error}"}, status=status.HTTP_400_BAD_REQUEST)
        except IOError:
            return Response({'error': 'Network: page is unavailable'}, status=status.HTTP_400_BAD_REQUEST)

        if r.status_code == 200:
            return Response({
                'title': self.parse_title(r.text),
                'content-type': r.headers['Content-Type'],
                'image': '/static/screenshots/screenshot.png',
                'description': self.parse_description(r.text),
                'lang': self.parse_lang(r.text)
            }, status=status.HTTP_201_CREATED)

    @staticmethod
    def parse_title(html):
        m = re.search(r'<title>(.+)</title>', html)
        return m.group(1)

    @staticmethod
    def parse_description(text):
        m = re.search(r'<meta property="og:description" content="(.+)" />', text)
        if m is not None and m.group(1) != "":
            return m.group(1)
        else:
            return "no description found"

    @staticmethod
    def parse_lang(html):
        m = re.search(r'<html lang="(.+)"', html)
        if m is not None and m.group(1) != "":
            return m.group(1)
        else:
            return "en"
