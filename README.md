# blink-spa-rest

bLink - сервис для обмена полезными ссылками, зарегистрированный пользователь может добавлять ссылки, проставлять теги, 
делиться по короткой ссылке, которую может создать сам или сгенерировать автоматически, редактировать название и описание 
ссылок, голосовать 1-5 звездочек, сервис выводит рейтинг и количество проголосовавших

Компоненты сервиса:
* front           - SPA vue.js

2 варианта backend:
* backend-perl    - REST-сервис на Mojolicious::Lite
* backend-python  - REST-сервис на Django REST framework


## How to run frontend application

```shell
cd ./frontend
npm i
npm run serve
```

### How to run frontend tests

```shell
npm run test:watch
```
---
## How to setup python-backend

```shell
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
python3 manage.py migrate
```

### How to run server

```shell
python3 manage.py runserver
```

### How to run server tests

```shell
python3 manage.py test
```
---

## How to setup backend-perl

```shell
cpan Mojolicious Mojo::SQLite IO::Socket::SSL
```

### How to run server
```shell
perl -Ilib bin/blinkapp.pl
```

### How to run tests
```shell
prove -Ilib
```