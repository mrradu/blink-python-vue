import Vue from "vue";
import Vuex from "vuex";
import linkBuilder from "./linkBuilder";

Vue.use(Vuex);

const clientMock = {
  createLink: jest.fn(() => Promise.resolve()),
};

const link = {
  url: "https://google.com",
  title: "Google",
  description: "Google search",
};

const linkBuilderState = {
  link,
  loading: false,
};

describe("linkBuilder", () => {
  describe("createLink", () => {
    test("given valid link sends request with link data and user id", () => {
      const store = new Vuex.Store({
        modules: {
          linkBuilder: {
            ...linkBuilder,
            state: linkBuilderState,
          },
        },
        state: {
          user: {
            name: "lu",
            id: "test-user",
          },
        },
      });
      store.$client = clientMock;

      store.dispatch("linkBuilder/createLink");

      expect(clientMock.createLink).toHaveBeenCalledWith(link, "test-user");
    });
  });
});
