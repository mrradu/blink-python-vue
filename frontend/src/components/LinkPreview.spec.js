import { shallowMount } from "@vue/test-utils";
import LinkPreview from "./LinkPreview.vue";

jest.useFakeTimers();

it("loads preview", async () => {
  const getPreviewMock = jest.fn();
  const url = "https://en.wikipedia.org/wiki/SOLID";
  const wrapper = shallowMount(LinkPreview, {
    propsData: { url: "" },
    provide: { client: { getPreview: getPreviewMock } },
  });
  wrapper.setProps({ url });
  await wrapper.vm.$nextTick(jest.runAllTimers);
  expect(getPreviewMock).toHaveBeenCalledWith(url);
});
