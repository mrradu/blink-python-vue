import { shallowMount } from "@vue/test-utils";
import CreateLinkForm from "./CreateLinkForm.vue";

describe("CreateLinkForm", () => {
  it("given empty URL renders disabled button", () => {
    const data = () => ({ url: "" });

    const wrapper = shallowMount(CreateLinkForm, { data });

    expect(wrapper.find(".submit-link").attributes().disabled).toBe("disabled");
  });

  it("given non-empty URL renders active button", () => {
    const data = () => ({ url: "test-url" });

    const wrapper = shallowMount(CreateLinkForm, { data });

    expect(
      wrapper.find(".submit-link").attributes().disabled
    ).not.toBeDefined();
  });
});
