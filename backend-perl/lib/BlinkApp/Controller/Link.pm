package BlinkApp::Controller::Link;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use Mojo::SQLite;

sub list($self) {
    my $user_id = undef;
    if(defined $self->param('user_id')) {
        $user_id = $self->param('user_id');
    } else {
        if (defined $self->req->headers->header('http_user_id')) {
            $user_id = $self->req->headers->header('http_user_id');
        } else {
            return $self->render(json => { errors => [ { HTTP_USER_ID => 'This header required.' } ] }, status => 403)
        }
    }

    my $result = $self->sqlite->db->select('links', ['*'], {user_id => $user_id})->hashes;

    $self->render(json => $result, status => 200);
}

sub create($self) {
    my $data = $self->req->json;

    my $user_id = $self->req->headers->header('http_user_id');

    return $self->render(json => { errors => [ { HTTP_USER_ID => 'This header required.' } ] }, status => 403)
        unless $user_id;

    $data->{'user_id'} = $user_id;

    return $self->render(json => { errors => [ { url => 'should be HTTPS' } ] }, status => 400)
        unless $data->{url} =~ qr/^https/i;

    return $self->render(json => { errors => [ { description => 'This field may not be null.' } ] }, status => 400)
        unless exists $data->{description};


    if (defined($data->{image_url})) {
        my $response = $self->ua->get($data->{image_url})->result;

        if ($response->code != 200) {
            $data->{image_url} = undef;
        }

        if (defined $response->headers->content_length && $response->headers->content_length > 1_048_576) {
            $data->{image_url} = undef;
        }
    }

    $data->{id} = $self->sqlite->db->insert('links', $data)->last_insert_id;

    $self->render(json => $data, status => 200)

}

1;