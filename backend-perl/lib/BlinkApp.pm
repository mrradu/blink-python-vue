package BlinkApp;
use Mojo::Base 'Mojolicious', -signatures;
use Mojo::SQLite;


sub startup($self) {
    my $config = $self->plugin('NotYAMLConfig');
    $self->secrets($config->{secrets});

    $self->helper(sqlite => sub {state $sqlite = Mojo::SQLite->new('sqlite:blink.sqlite3')});

    my $r = $self->routes;
    $r->get('/links/')->to('Link#list');
    $r->post('/links/')->to('Link#create');
}

1